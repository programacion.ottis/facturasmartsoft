import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Factura } from '../Modelo/Factura';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  
  constructor(private http:HttpClient) { }

  url='http://localhost:8080/smartsoft/facturas/all'
  urlAdd='http://localhost:8080/smartsoft/facturas'
  urlFindId=''
  getFacturas(){
    return this.http.get<Factura[]>(this.url);
  }
  createFactura(factura:Factura){
    return this.http.post<Factura>(this.urlAdd,factura)

  }
  getFacturasById(id:number){
    return this.http.get<Factura>(this.urlFindId)
  }
}
