import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Factura } from 'src/app/Modelo/Factura';
import {ServiceService} from "../../Servise/service.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  editFactura(){
    
  }
  factura:Factura[];
  constructor(private service:ServiceService ,private router:Router) { }

  ngOnInit(): void {
    this.service.getFacturas()
    .subscribe(data=>{
      this.factura=data;
    })
  }

}
