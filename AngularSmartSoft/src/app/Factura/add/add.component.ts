import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Factura } from 'src/app/Modelo/Factura';
import { ServiceService } from 'src/app/Servise/service.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  factura:Factura;
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
  }

  guardar(factura:Factura){
    this.service.createFactura(factura)
    .subscribe(data=>{
      alert("Se agrego Correctamente");
      this.router.navigate(["list"]);
    })
  }

}
