import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PruebaSmartSoft';

  constructor(private router:Router){}

  listFactura(){
    this.router.navigate(['list']);
  }
  addFactura(){
    this.router.navigate(['add']);
  }
  editFactura(){
    this.router.navigate(['edit']);
  }
  // delFactura(){
  //   this.router.navigate(['list']);
  // }
}
