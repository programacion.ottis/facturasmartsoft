import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './Factura/list/list.component';
import { AddComponent } from './Factura/add/add.component';
import { EditComponent } from './Factura/edit/edit.component';

const routes: Routes = [
  {path:'list', component: ListComponent},
  {path:'add', component: AddComponent},
  {path:'edit', component: EditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
