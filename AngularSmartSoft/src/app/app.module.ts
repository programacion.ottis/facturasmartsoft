import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './Factura/list/list.component';
import { AddComponent } from './Factura/add/add.component';
import { EditComponent } from './Factura/edit/edit.component';
import { FormsModule } from '@angular/forms';
import { ServiceService } from '../app/Servise/service.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    AddComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ServiceService,HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
