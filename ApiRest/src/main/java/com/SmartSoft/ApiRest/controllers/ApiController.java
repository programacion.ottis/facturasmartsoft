package com.SmartSoft.ApiRest.controllers;

import com.SmartSoft.ApiRest.dto.FacturasDTO;
import com.SmartSoft.ApiRest.entities.FacturasModel;
import com.SmartSoft.ApiRest.services.implentation.FacturasImplements;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//Clase de control, para las rutas definidas para hacer uso de los metodos del CRUD 
//@CrossOrigin  lo definimos para atender solicitudes de origen cruzado o con un origen diferente
@CrossOrigin("*")
//Se define la funcionalidad como tipo Rest
@RestController
//Se mapea con la ruta por defecto /facturas
@RequestMapping("/facturas")
public class ApiController {
    //La notacion Autowired nos permitira inyectar unas dependecias a otras
    @Autowired
    FacturasImplements facturasImplements;

    //Se mapea con la ruta all para acceder al metodo findAll el cual retornara una lista con todas las facturas
    @GetMapping(value= "all", produces= MediaType.APPLICATION_JSON_VALUE)
    public List<FacturasModel> listar(){
        
        return facturasImplements.findAll();
    }

    //Se mapea con la ruta /facturas pero con metodo post el cual se usa para realizar el envio del cuerpo de datos
    @PostMapping()
    public FacturasModel add(@RequestBody FacturasModel fModel){
        
        return facturasImplements.save(fModel);
    }

    //Metodo usado para realizar una busqueda por el numero de factura usando la ruta findID
    @GetMapping(value= "findId", produces= MediaType.APPLICATION_JSON_VALUE)
    public FacturasModel findById(int num_factura){
        
        return facturasImplements.findById(num_factura);
    }
    


    @GetMapping("index")
    public ResponseEntity<Object> index(){
        return ResponseEntity.ok("OK");
    }
    
}
