package com.SmartSoft.ApiRest.entities;
import java.time.LocalDate;
import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;


//Se define la Entidad Factura asociado a la tabla factura de la base de datos
@Entity
@Table(name="factura")
public class FacturasModel {

    //Variables de clase mapeadas hacia las columnas de la tabla
    @Id
    @Column(name = "num_factura")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int num_factura;
    @Column(name="id_cliente")
    private int id_cliente;
    @Column(name="fecha",columnDefinition = "DATE")
    private LocalDate fecha;


    //Metodos Get y Set para cada una de las variables

    public int getnum_factura() {
        return num_factura;
    }
    public void num_factura(int num_factura) {
        this.num_factura = num_factura;
    }
    public int getId_cliente() {
        return id_cliente;
    }
    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }
    public LocalDate getFecha() {
        return fecha;
    }
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    //Metodo para ingresar la fecha automaticamente
    // @PrePersist
    // public void prePersist(){
    //     this.fecha=new Date();
    // }
    

}