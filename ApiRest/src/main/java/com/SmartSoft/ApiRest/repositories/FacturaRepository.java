package com.SmartSoft.ApiRest.repositories;

import java.util.List;

import com.SmartSoft.ApiRest.entities.FacturasModel;

import org.springframework.data.repository.CrudRepository;


public interface FacturaRepository extends CrudRepository<FacturasModel,Integer>{

    //Metodo findAll desde la interfaz FactuaService
    List<FacturasModel> findAll();

    //Los otros Metodos vienen desde CrudRepository
      

    
}
