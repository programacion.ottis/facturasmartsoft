package com.SmartSoft.ApiRest.services.implentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.SmartSoft.ApiRest.dto.FacturasDTO;
import com.SmartSoft.ApiRest.entities.FacturasModel;
import com.SmartSoft.ApiRest.repositories.FacturaRepository;
import com.SmartSoft.ApiRest.services.interfaces.FacturaService;
import com.SmartSoft.ApiRest.utils.MHelper;

import org.modelmapper.internal.bytebuddy.asm.Advice.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

//Definimos esta clase como componente para que el Spring pueda agregarla al contexto de la aplicación
@Component
public class FacturasImplements implements FacturaService {

    //La notacion Autowired nos permitira inyectar dependencias a FacturaModel
    @Autowired
    private FacturaRepository facturaRepository;

    //Se usa la dependencia Override para sobreescribir el metodo implementado por la interfaz
    // @Override
    // public List<FacturasDTO> findAll() {
    //     List<FacturasDTO> dto = new ArrayList<>();
    //     Iterable<FacturasModel> facturas = this.facturaRepository.findAll();

    //     for (FacturasModel facturasModel : facturas) {
    //         FacturasDTO facDTO= MHelper.modelMapper().map(facturasModel,FacturasDTO.class);
    //         dto.add(facDTO);
    //     }
    //     return dto;
    // }

    // private FacturasDTO convertToFacturaDTO(final FacturasModel facturas){
    //     return MHelper.modelMapper().map(facturas, FacturasDTO.class);   
    // }


    //con la notacion Override Sobreescribimos el metodo findAll y retorna una lista de todas las facturas en la base de datos facturas 
    @Override
    public List<FacturasModel> findAll() {
        
        return facturaRepository.findAll();
    }

    // @Override
    // public FacturasDTO findByFacturaId(int num_factura) {
    //     Optional<FacturasModel> facturas = this.facturaRepository.findById(num_factura);

    //     return  MHelper.modelMapper().map(facturas.get(), FacturasDTO.class);
    // }

    //Sobreescribimos el metodo y retornamos un valor de tipo factura para comprobar que se almaceno
    @Override
    public FacturasModel save(FacturasModel factura) {

        facturaRepository.save(factura);
        return factura;
        // FacturasModel facturaNew=MHelper.modelMapper().map(factura, FacturasModel.class);
        // this.facturaRepository.save(facturaNew);
        
    }

    //Sobreescribimos el metodo findById y retorna un objeto de tipo factura
    @Override
    public FacturasModel findById(int num_factura) {
        Optional<FacturasModel> factura = this.facturaRepository.findById(num_factura);
        return  MHelper.modelMapper().map(factura, FacturasModel.class);
    }

    // @Override
    // public void saveAll(List<FacturasDTO> facturas) {

        
    //     List<FacturasModel> fac = new ArrayList<>();

    //     for (FacturasDTO f : facturas) {
    //         FacturasModel factura= MHelper.modelMapper().map(f, FacturasModel.class);
    //         fac.add(factura);
    //     }

    //     this.facturaRepository.saveAll(fac);
        
    // }

    // @Override
    // public void deleteById(int num_factura) {
    //     this.facturaRepository.deleteById(num_factura);
        
    // }


}
