package com.SmartSoft.ApiRest.services.interfaces;

import java.util.List;
import com.SmartSoft.ApiRest.dto.*;
import com.SmartSoft.ApiRest.entities.FacturasModel;
import org.springframework.stereotype.Service;

//En esta interfaz se crean los metodos que se van a utilizar para el CRUD

@Service
public interface FacturaService {

    //Metodo findAll lista todos los elementos de la tabla factura de la base de datos
    List<FacturasModel> findAll();
    //Metodo Save registra en la base de datos factura
    FacturasModel save(FacturasModel factura);
    //Metodo findById funciona para traer la entidad factura que se encuentre dentro de la tabla factura de la base de datos
    FacturasModel findById(int num_factura);


    
}
