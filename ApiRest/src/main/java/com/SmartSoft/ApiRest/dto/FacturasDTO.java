package com.SmartSoft.ApiRest.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class FacturasDTO implements Serializable {
    
    private int id_factura;
    private int id_cliente;
    private Date fecha;

}
